//====================================================================
//
// ppm-siegfred
// Author: J-dar Siegfred Rodriguez
// Date  : 12/08/2017
//
//    This application processes a Binary PPM Image file.
//    All processes are created to achieve just current
//    application goal: Rotate and Grayscale the image,
//    but was also designed to be extended.
//
//    All methods expects PPMImage objects are created via
//    ppm_Create() constructor function. otherwise, behaviour
//    is undefined.
//
//    A successful call returns a positive Byte,
//    zero indicates failure.
//
//====================================================================
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned int UInt;
typedef unsigned char Byte;

typedef struct {
  UInt width;
  UInt height;
  Byte *pixels;
  Byte maxGray;
} PPMImage;

//====================================================================
//
// stripInvalidData
//
// parameter/s :
//   FILE object
//
// return :
//   None.
//
//  Purges unnecessary data such as
//  Whitespaces and comments from the file stream.
//
//====================================================================
void stripInvalidData(FILE* file)
{
  Byte byte;

	fscanf(file, " %c", &byte);
	while (byte == '#') {
		fscanf(file, "%*[^\n]\n", NULL);
		fscanf(file, " %c", &byte);
	}
	ungetc(byte, file);
}

//====================================================================
//
// ppm_Destroy
//
// parameter/s :
//   PPM image object
//
// return :
//   None.
//
//  Destroys PPMImage objects created from ppm_Create.
//
//====================================================================
void ppm_Destroy(PPMImage* image)
{
    free(image->pixels);
    free(image);
}

//====================================================================
//
// ppm_Create
//
// parameter/s :
//   string file_name / path
//
// return :
//   Pointer to PPM image object
//
//  Creates an PPM image object from given file.
//  Does all the validation check for the image,
//  any object returned from this constructor
//  is relatively correct.
//
//====================================================================
PPMImage* ppm_Create(char* file_name)
{
  FILE* file;
  PPMImage* image = NULL;
  UInt size;

  if (file_name == NULL) { return 0; }

  file = fopen(file_name, "rb");
  if (file == NULL) { return NULL; }

  stripInvalidData(file);
  if (fgetc(file) != 'P' || fgetc(file) != '6') { return NULL; }

  image = (PPMImage*)calloc(1,sizeof(PPMImage));
  stripInvalidData(file);
  if (fscanf(file, "%u %u", &(image->width), &(image->height)) != 2) {
    ppm_Destroy(image);
    return NULL;
  }

  stripInvalidData(file);
  if (fscanf(file, "%hhu", &(image->maxGray)) != 1) {
    ppm_Destroy(image);
    return NULL;
  }

  stripInvalidData(file);
  size = image->width * ((image->height << 1) + image->height);
  image->pixels = (Byte*)malloc(size);

  if (fread(image->pixels, sizeof(Byte), size, file) != size) {
    ppm_Destroy(image);
    return NULL;
  }

  fclose(file);
  return image;
}

//====================================================================
//
// ppm_WriteToFile
//
// parameter/s :
//   PPMImage pointer p
//   string file_name / path
//
// return :
//   Byte status
//
//  Writes an image to a file, failure can only be caused
//  IO failure or bad file_name parameter.
//
//====================================================================
Byte ppm_WriteToFile(PPMImage* image, char* file_name)
{
  FILE* file;

  if (file_name == NULL)
  { return 0; }

  file = fopen(file_name, "wb");

  if (file == NULL)
  { return 0; }

  fprintf(file, "P6\n%u %u\n%u\n", image->width, image->height, image->maxGray);
  fwrite(image->pixels, sizeof(Byte), image->width * ((image->height << 1) + image->height), file);

  fclose(file);
  return 1;
}

//====================================================================
//
// contestProcess
//
// parameter/s :
//   PPMImage pointer p
//
// return :
//   Byte status
//
//  Will execute required image processes for this contest.
//  For now, this function rotates and performs grayscale.
//
//====================================================================
Byte contestProcess(PPMImage* image)
{
  Byte* new_image = NULL;                                   // new canvas.
  Byte gray = 0;                                            // grayscale holder for pixel.
  UInt stride = (image->width << 1) + image->width;         // image width * 3, this the image's data width per row.
  UInt size = stride * image->height;
  UInt offset = 0;                                          // offset of next column to transfer from source image.
  UInt temp = 0;                                            // used for swapping width and height.
  const UInt base_src_index = size - stride;        // lower-left most pixel of the source image.
  int src_index = size;                             // source image index to copy
  int i;

  new_image = (Byte*)malloc(size);
  if (new_image == NULL) { return 0; }              // if allocation fails. return non-zero.

  for (i = 0; i < size; i += 3) {                   // loop through each pixel in the
    src_index -= stride;                            // new canvas, calculate which
                                                    // pixel from the source should be
    if (src_index < 0) {                            // transferred, then convert it to
      src_index = base_src_index + offset;          // grayscale before transferring.
      offset += 3;
    }

    gray = (((image->pixels[src_index] + image->pixels[(src_index+1)]) >> 1) + image->pixels[(src_index+2)]) >> 1;
    memset(&new_image[i], gray, 3);
  }

  temp = image->height;                             // setting new values and freeing old canvas
  image->height = image->width;
  image->width  = temp;
  free(image->pixels);
  image->pixels = new_image;

  return 1;
}

//====================================================================
//
// main
//
//  This is main.
//        - Sherlock
//
//====================================================================
int main(int argc, char* argv[])
{
  PPMImage* image;
  char output[2048] = {'\0'};

  if (argc <= 1 ) {
    printf("Usage: ppm-siegfred <image URL>\n");
    return 0;
  }

  image = ppm_Create(argv[1]);                      // only care about first argument for now.

  if (image == NULL) {
    printf("Cannot open file : %s\n", argv[1]);
    return 0;
  }

  strcat(output, argv[1]);
  strcat(output, ".out");

  if (!contestProcess(image) || !ppm_WriteToFile(image, output)) {
      printf("Processing failed\n");
  }

  ppm_Destroy(image);

  return 1;
}
